# チャットアプリ
動画教材をベースにLINE風チャットアプリを作成。
ActionCableを使い、リアルタイム通信を行なっています。

## URL
<https://secret-cliffs-56767.herokuapp.com/>


## 残りの課題
* ユーザー認証
* メッセージを送信したときに、自動で最下部にスクロールする。
